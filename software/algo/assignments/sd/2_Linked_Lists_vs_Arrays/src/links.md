## Weblinks
+ Assignment code can be found in git.fhict.nl repository t-sem3-cb-code
+ [Pointer and Dynamic Memory Management](https://www.youtube.com/watch?v=_8-ht2AKyH4)
+ [Linked List: Implementation in C/C++](https://www.youtube.com/watch?v=vcQIFT79_50)
+ [Memory Management in C Tutorial](https://www.tutorialspoint.com/cprogramming/c_memory_management.htm)
