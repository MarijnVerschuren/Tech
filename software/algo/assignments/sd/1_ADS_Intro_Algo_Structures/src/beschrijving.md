This module deals with analysis of complexity and performance of different algorithms. How is the performance of algorithms influenced and measured? The basics of Big O notation are explained.

