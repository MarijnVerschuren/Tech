## Weblinks

+ Assignment code can be found in git.fhict.nl repository t-sem3-cb-code
+ [(Almost) math-free explanation of Big O notation](https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/)
+ [15 Sorting Algorithms in 6 Minutes](https://www.youtube.com/watch?v=kPRA0W1kECg&feature=youtu.be)
+ [ A Gentle Explanation of Logarithmic Time Complexity](https://medium.com/better-programming/a-gentle-explanation-of-logarithmic-time-complexity-79842728a702)
