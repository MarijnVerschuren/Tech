## Weblinks

+ [How To Structure C++ Program in .cpp and .h files](https://www.youtube.com/watch?v=pvzT4hIVE4s)
+ [C++ Deep and Shallow Copy Animated](https://www.youtube.com/watch?v=C_nLA3hfw8E)
+ [C++ Pass by Value/Pass by Reference Animated](https://www.youtube.com/watch?v=ErMKBh1pobg)
