## Weblinks

+ [UML@Classroom, Martina Seidl, Marion Scholz, Christian Huemer, Gerti Kappel., Springer Verlag, 2015.](http://www.uml.ac.at/en/)
+ [Inspiration video: Object Oriented Design of an Elevator](https://www.youtube.com/watch?v=5Ogjof9g5T0)
+ [Google c++ Test Mocking Framework Part - 4: Writing Mocks using c++ gtest and c++ gmock](https://youtu.be/dLB2aDasVTg)

