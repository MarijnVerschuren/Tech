## Weblinks

+ [C++ Pass by Value/Pass by Reference Animated](https://www.youtube.com/watch?v=ErMKBh1pobg)
+ [C++ Templates and Iterators](https://youtu.be/ywNZAebgEZw)
+ [Sequence containers](https://www.youtube.com/watch?v=KJWOm1bXUxM&list=PLGLfVvz_LVvQ9S8YSV0iDsuEU8v11yP9M&index=20)
+ [Associative Containers and Container Adapter](https://youtu.be/IaTzi_bsuJU?list=PLGLfVvz_LVvQ9S8YSV0iDsuEU8v11yP9M)