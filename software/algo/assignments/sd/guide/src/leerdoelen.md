## Leerdoelen

Er is een onderverdeling gemaakt tussen algemene leerdoelen en leerdoelen per module. De andere leerdoelen vind je in de module.

### 1-ADS-Intro-algo-structures

+ Student can differentiate algorithms based on their compexity and performance
+ Student can express algorithm performance with Big O notation

### 2-ADS-Linked-lists-vs-Arrays

+ Student can explain difference between arrays and linked lists
+ Student can make choice between array and linked list implementation for different use cases
+ Student can implement linked lists

### 3-ADS-Stacks-Queues

+ Student can explain difference between LIFO and FIFO data structures
+ Student can implement and use queues and stacks

### 4-ADS-Graphs-Trees-Recursion

+ Student can explain difference between trees and graphs
+ Student can implement and use Breadth-First and Depth-First search algorithms
+ Student can implement recursive functions.

